mod cli;
mod real {
    pub mod ledger;
    pub mod handlers {
        pub mod fs;
        pub mod git;
        pub mod gpg;
    }
}

use clap::Parser;
use cli::Arguments;
use cli::Commands;

use real::ledger::{ Ledger, LedgerOpts };
use real::handlers::{
    git::RealGitHandler, 
    fs::RealFileHandler
};

use crate::cli::LedgerCommands::Open;
use crate::cli::LedgerCommands::New;

// TODO: Implement init of real default environment. Defaults to .real
// TODO: Implement init of git environment
// TODO: Implement encryption

fn main() {

    let args = Arguments::parse();

    println!("{:?}", args);

    match args.command {
        Commands::Ledger(subcommand) => {
            println!("{:?}: In ledger!", subcommand);
            match subcommand.command {
                Open{name} => { 
                    println!("{:?} In subcommand open!", name);
                    let ledger = Ledger::new(
                        name,
                        RealFileHandler{
                            base_path: String::from("./"),
                            editor_path: String::from("path")
                        },
                        LedgerOpts {
                            git_handler: None
                        }
                    );

                    ledger.create();
                },
                New{name} => {
                    println!("{:?} In subcommand new!", name);
                },
            }
        },
        Commands::Git(_) => {
            println!("In git!")
        },
        _ => todo!(),
   }
}
