use clap::{Args, Parser, Subcommand};

#[derive(Debug, Parser)]
#[command(name = "real")]
#[command(about = "A CLI finance management tool")]
pub struct Arguments {
    #[command(subcommand)]
    pub command: Commands
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    #[command(about = "Initialize git environment")]
    Git(GitArgs),
    #[command(arg_required_else_help = true, about = "Manage ledgers")]
    Ledger(LedgerArgs),
}

#[derive(Debug, Args)]
pub struct GitArgs {
    #[command(subcommand)]
    pub command: Option<GitCommands>
}

#[derive(Debug, Subcommand)]
pub enum GitCommands {
    #[command(about = "Path to directory")]
    Path {
        #[arg(short, long, value_name = "init_path")]
        path: String
    }
}

#[derive(Debug, Args)]
pub struct LedgerArgs {
    #[command(subcommand)]
    pub command: LedgerCommands,
}

#[derive(Debug, Subcommand)]
pub enum LedgerCommands {
    #[command(arg_required_else_help = true, about = "Create new ledger")]
    New {
        #[arg(short, long, value_name = "name")]
        name: String
    },
    #[command(arg_required_else_help = true, about = "Open existing ledger")]
    Open {
        #[arg(short, long, value_name = "name")]
        name: String
    },
}
