use std::{
    fmt,
    env::var,
    error::Error
};

const REAL_BASE_PATH: &str = "REAL_BASE_PATH";
const REAL_OVERRIDE_EDITOR: &str = "REAL_OVERRIDE_EDITOR";
const REAL_OVERRIDE_GPG_KEY: &str = "REAL_OVERRIDE_GPG_KEY";
const REAL_GPG_OPTS: &str = "REAL_GPG_OPTS";

#[derive(Debug)]
pub struct RealEnv {
    pub base_path: Box<str>,
    pub editor_path: Box<str>,
    pub gpg_key: Option<Box<str>>,
    pub gpg_opts: Option<Box<str>>,
}

impl RealEnv {

    pub fn build() -> Result<Self, Box<dyn Error>> {
        Ok(RealEnv {
            base_path: match var(REAL_BASE_PATH) {
                Ok(path) => path.into(),
                Err(_) => format!("{}/.real", var("HOME")?).into()
            },
            editor_path: match var(REAL_OVERRIDE_EDITOR) {
                Ok(path) => path.into(),
                Err(_) => var("EDITOR")?.into()
            }, 
            gpg_key: match var(REAL_OVERRIDE_GPG_KEY) {
                Ok(key) => Some(key.into()),
                Err(_) => None
            },
            gpg_opts: match var(REAL_GPG_OPTS) {
                Ok(opts) => Some(opts.into()),
                Err(_) => None
            }
        })
    }
}
