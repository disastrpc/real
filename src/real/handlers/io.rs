use std::{
    fmt,
    path::Path,
    io::Read,
    process::Command,
    fs::{
        File, 
        create_dir_all, 
    },
};

use std::io::Error;

use log::{debug, warn, error};
pub struct IOHandler {
    pub base_path: Box<str>,
    pub editor_path: Box<str>
}

impl IOHandler {

    fn ensure_full_path(&self, path: &Path) -> Result<(), Error>{

        match path.try_exists() {
            Ok(true) => { debug!("Path {:?} exists, not creating...", path) },
            Ok(false) => {   

                match path.parent() {
                    Some(prefix) => match create_dir_all(prefix) {
                        Ok(_) => {},
                        Err(err) => { return Err(err) }
                    },
                    None => {}
                };
            
            },
            Err(err) => { return Err(err) }
        };

        Ok(())
    }

    pub fn exists(&self, path: Box<str>) -> Result<bool, Error> {
        Path::new((*path).into()).try_exists()
    }

    /// Open the handler's file path using the editor defined by _$EDITOR_
    pub fn create(&self, path: Box<str>) -> Result<(), Error> {

        let full_path_str = format!("{}/{}", self.base_path, path); 
        
        let path = Path::new(full_path_str.as_str());
        debug!("Generated full path {:?}", path);

        self.ensure_full_path(path)?;

        match File::create(path) {
            Ok(_) => { Ok(()) },
            Err(err) => { Err(err) }
        }
    }
}
