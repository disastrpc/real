use std::error::Error;

use crate::{
    IOHandler,
    GitHandler
};

pub struct LedgerOpts {
    pub git_handler: Option<GitHandler>,
}

pub struct Ledger { 
    pub path: Box<str>,
    pub file_handler: IOHandler,
    pub opts: LedgerOpts
}

impl Ledger {
    
    pub fn new(path: Box<str>, file_handler: IOHandler, opts: LedgerOpts) -> Self { 
        Self { path, file_handler, opts }
    }

    pub fn exists(&self) -> Result<bool, Box<dyn Error>> {
        Ok(self.file_handler.exists(self.path.clone())?)
    }

    pub fn open(&self) {

    }

    pub fn create(&self) {
        let _ = self.file_handler.create(self.path.clone());
    }
}
